#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataScraping.h"

void optionsMenu(){
    puts("0 - Sair");
    puts("1 - Bicicletas Encontradas: Mostra todas as bicicletas encontradas durante o processamento dos arquivos de log"); 
    puts("2 - Informe uma das bicicletas encontradas, apresenta a lista de atividades, resumo");
    puts("3 - Lista todas atividades agrupadas por bicicleta e ordenadas pela data");
    puts("4 - Lista todas atividades agrupadas por bicicleta e ordenadas pela distância");
    puts("5 - Lista todas atividades ordenadas pela subida acumulada");
    puts("6 - Histograma: O usuário deve escolher uma bicicleta e apresentar um histograma da seguinte forma:");
}


/* Sort */

/* Interface qsort() da stdlib*/
int compareGear(const void *a, const void *b) {
   sLog *Bike1 = *(sLog**)a; 
   sLog *Bike2 = *(sLog**)b;
   return strcmp(Bike1->gear, Bike2->gear);
}

int compareDistance(const void *a, const void *b) {
   sLog *Bike1 = *(sLog**)a; 
   sLog *Bike2 = *(sLog**)b;
   return (Bike1->distance > Bike2->distance ? 1 : -1);
}

int compareDate(const void *a, const void *b){
   sLog *Bike1 = *(sLog**)a; 
   sLog *Bike2 = *(sLog**)b;
   int d1, d2;
   int a10, a11, a20, a21;
   sscanf(Bike1->date,"%d/%d", &a10, &a11);
   d1 = a10 + 12*a11;
   
   sscanf(Bike2->date,"%d/%d", &a20, &a21);
   d2 = a20 + 12*a21;
   return (d1 > d2);

}

int compareAscent(const void *a, const void *b) {
   sLog *Bike1 = *(sLog**)a; 
   sLog *Bike2 = *(sLog**)b;
   return (Bike1->ascent > Bike2->ascent ? 1 : -1);
}
/*Menu de opcoes*/
//1
void bikesFound(sLog **LogsArray, int size){
    char* BufBikeName;
    int i;

    qsort(LogsArray, size, sizeof(sLog*), compareGear); //Ordenando as bikes
    BufBikeName = (*LogsArray)->gear;
    printf("Bicicletas encontradas:\n - %s\n", BufBikeName);
    for(i = 0; i < size; i++){
        if ( strcmp(BufBikeName, LogsArray[i]->gear) != 0){ //Nova bicicleta encontrada
            BufBikeName = LogsArray[i]->gear; 
            printf(" - %s\n", BufBikeName);
        }
    } putchar('\n');
}

//2
void resumeBike(sLog **LogsArray, int size){

    char buffer[1024];
    printf("Digite o nome da Bike:");
    scanf(" %[^\n]", buffer);
    qsort(LogsArray, size, sizeof(sLog**), compareGear);
    
    int i = 0;
    while (i < size && strcmp(buffer, LogsArray[i]->gear) != 0) i++;
       
    if ( i < size){ //Achou o inicio do indice da bike
        printf("\nBicicleta: %s\n", LogsArray[i]->gear);
        printf("Data Distancia(km) Velocidade Media (km/h) Velocidade Maxima (Km/h) HR Medio (bpm) HR Maximo (bpm) Cadencia Media (rmp) Subica Acumulada (m)\n");
        while (i < size && strcmp(buffer, LogsArray[i]->gear) == 0){
            printLog(LogsArray[i]);
            i++;
        } printf("\n");
    } else printf("Bicicleta nao encontrada\n");

}

//3
void resumeAllDate(sLog **LogsArray, int size){

    qsort(LogsArray, size, sizeof(sLog*), compareGear); //Organiza por Bike

    int right = 0, left = 0, i;
    char* buffer;
    buffer =  LogsArray[right]->gear;
    while (right <= size){
        while (right<size && strcmp(buffer, LogsArray[right]->gear) == 0) right++;

        qsort(LogsArray+left, right-left, sizeof(sLog**), compareDate);//Ordenando por distancia por bike

        printf("Bicicleta %s\n", LogsArray[left]->gear);
        printf("Data  Distancia(km) Velocidade Media (km/h) Velocidade Maxima (Km/h) HR Medio (bpm) HR Maximo (bpm) Cadencia Media (rmp) Subica Acumulada (m)\n");
        for (i = left; i < right; i++) printLog(LogsArray[i]); //Imprimindo os logs ordenados
        putchar('\n');

        if (right < size) buffer = LogsArray[right]->gear;

        left = right; right++;
    }
}

//4 - identica a 3, so precisa criar um function poointer para distance / date
void resumeAllDistance(sLog **LogsArray, int size){
    
    qsort(LogsArray, size, sizeof(sLog**), compareGear); //Organiza por Bike

    int right = 0, left = 0, i;
    char* buffer;
    buffer =  LogsArray[right]->gear;
    while (right <= size){
        while (right<size && strcmp(buffer, LogsArray[right]->gear) == 0) right++;

        qsort(LogsArray+left, right-left, sizeof(sLog**), compareDate);//Ordenando por distancia por bike

        printf("Bicicleta %s\n", LogsArray[left]->gear);
        printf("Data  Distancia(km) Velocidade Media (km/h) Velocidade Maxima (Km/h) HR Medio (bpm) HR Maximo (bpm) Cadencia Media (rmp) Subica Acumulada (m)\n");
        for (i = left; i < right; i++) printLog(LogsArray[i]); //Imprimindo os logs ordenados
        putchar('\n');

        if (right < size) buffer = LogsArray[right]->gear;

        left = right; right++;
    }
}

//5
void resumeAllAscent(sLog **sLogs, int nFiles){
    int i;

    qsort(sLogs, nFiles, sizeof(sLog*), compareAscent);
    printf("Data  Distancia(km) Velocidade Media (km/h) Velocidade Maxima (Km/h) HR Medio (bpm) HR Maximo (bpm) Cadencia Media (rmp) Subica Acumulada (m)\n");
    for(i = 0; i < nFiles; i++)
        printLog(sLogs[i]);
}

//6 
int printHistogram(sLog **LogsArray, int size){

    char buffer[1024];
    printf("Digite o nome da bike:");
    scanf(" %[^\n]", buffer);
    printf("%s\n", buffer);

    qsort(LogsArray, size, sizeof(sLog**), compareGear); //Organiza por Bike

    int right = 0, left = 0;

    while (left<size && strcmp(buffer, LogsArray[left]->gear) != 0) left++;

    if (left >= size){
        printf("invalida\n");
        return -1;
    }

    right = left;
    while (right<size && strcmp(buffer, LogsArray[right]->gear) == 0) right++;

    qsort(LogsArray+left, right-left, sizeof(sLog**), compareDistance);//Ordenando por distancia por bike


    int i, dist;
    dist = 29;
    printf("%3.d - %3.d|", 1, dist);
    for(i = left;  i<right; i++){
        if (LogsArray[i]->distance > dist){
            while (LogsArray[i]->distance > dist){
                dist += 10; putchar('\n');
                printf("%3.d - %3.d|",dist-10,dist);
            } 
        }
        printf("*");
    }
    printf("\n          123456789#123456789#123456789#");
    printf("\nDistancia|            Quantidade\n\n");
    return 0;


}


