#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#include "dataScraping.h"
#include "parse.h"
#include "menu.h"

#define sizeD_name 256 //Tam Maximo dirent->d_name[]

int main(int argc, char **argv){

    char *pathLogs = parsingArgument(argc, argv); 
    int nFiles, i, sizePath = strlen(pathLogs) + 1;
    
    char Buff[sizeD_name+sizePath]; strcpy(Buff, pathLogs); strcat(Buff, "/");

    DIR *dirLogs = opendir(pathLogs);
    struct dirent **fileLogs;
    FILE *streamLog;
    sLog **sLogs;

    if (dirLogs == NULL){
        fprintf(stderr, "Diretorio Invalido\n");
        return 1;
    }

    /* Sumarizacao dos logs */
    nFiles = scandir(pathLogs, &fileLogs, logExtension, NULL); //Filtra os arquivos .log
    sLogs = malloc(sizeof(sLog)*nFiles);
    for (i = 0; i < nFiles; i++){
        Buff[sizePath] = '\0'; strcat(Buff, fileLogs[i]->d_name); //Inicializa um Path relativo do arquivo
        streamLog = fopen(Buff,"r");
        sLogs[i] = readLog(streamLog);
        fclose(streamLog);
        
        printf("\rSumarizados %d de %d ", i+1, nFiles); fflush(stdout); //Perfumaria
    }
    putchar('\n');

    if (!nFiles){
        puts("Nenhum arquivo valido encontrado\n");
        free(sLogs);
        free(fileLogs);
        closedir(dirLogs);
        return 1;
    }

    /* Logs Sumarizados - Consultando os mesmos */
    int opt;
    enum ESTADO {SAIDA, ENCONTRADAS, RESUMO, DATA, DISTANCIA, SUBIDA, HISTOGRAMA};
    optionsMenu(); putchar('\n');
    do {
        printf("\nOpcao: "); scanf("%d", &opt); //scanf eh perigoso
        putchar('\n'); //Como o '\n' pode destruir o scanf? estudar stdin unix

        switch (opt) {
            case SAIDA: opt = 0; break;
            case ENCONTRADAS: bikesFound(sLogs, nFiles); break;
            case RESUMO: resumeBike(sLogs, nFiles); break;
            case DATA: resumeAllDate(sLogs, nFiles); break;
            case DISTANCIA: resumeAllDistance(sLogs, nFiles); break;
            case SUBIDA: resumeAllAscent(sLogs, nFiles); break;
            case HISTOGRAMA: printHistogram(sLogs, nFiles); break;
            default: puts("Opcao Invalida"); optionsMenu(); break;
        }

    } while (opt);
    
    /* Liberando Memoria */
    for (i = 0; i < nFiles; i++) { 
        freeLog(sLogs[i]);
        free(fileLogs[i]);
    }

    free(fileLogs);
    free(sLogs);
    closedir(dirLogs);

    return 0;
}

