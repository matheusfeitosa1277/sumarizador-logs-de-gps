#ifndef MENU_H
#define MENU_H

/* Menu */
void optionsMenu();

/* Sort */
void bikesFound(sLog **sLogs, int nFiles);
void resumeBike(sLog **sLogs, int nFiles);
void resumeAllDate(sLog **sLogs, int nFiles);
void resumeAllDistance(sLog **sLogs, int nFiles);
void resumeAllAscent(sLog **sLogs, int nFiles);
void printHistogram(sLog **sLogs, int nFiles);

#endif

