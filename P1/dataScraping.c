#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include "dataScraping.h"
#define LINESIZE  1024

/* Pre-processamento do Log */

int logExtension(const struct dirent *entry){
    char *ext = strrchr(entry->d_name, '.');

    if (ext != NULL)
        return !strcmp(ext, ".log");
    return 0;
}

/* Struct Log */
sLog *initLog(){
    sLog *log = malloc(sizeof(sLog));
    log->gear = NULL; log->date = NULL;

    log->distance = log->ascent = log->avgSpeed = log->maxSpeed = 0; //double
    log->avgHr = log->maxHr = log->avgCad = 0; //int

    return log;
}

void freeLog(sLog *log){
   if (log->gear != NULL) free(log->gear);
   if (log->date != NULL) free(log->date);

   free(log);
}

void printLog(sLog *log){
    printf("%-10s ", log->date);
    printf("%-13.2f ", log->distance);
    printf("%-23.2f ", log->avgSpeed);
    printf("%-24.2f ", log->maxSpeed);
    printf("%-14d ", log->avgHr);
    printf("%-15d ", log->maxHr);
    printf("%-20d ", log->avgCad);
    printf("%-21.2f\n", log->ascent);
}


/* Leitura da Stream - Data Scraping */
sLog *readLog(FILE *stream){
    sLog *log = initLog();

    readHeader(stream, log);
    readRegister(stream, log);

    return log;
}

//Cabecarios - 2 Primeras linha, Data e Gear
void readHeader(FILE *stream, sLog *log){
    char lineBuf[LINESIZE]; char *cBuf;

    char **Buf[] = {&log->gear, &log->date}; //Passagem por copia sempre, cuidado
    for (int i = 0; i <= 1; i++){ //BikeName && Date
        fgets(lineBuf, LINESIZE, stream);  
        cBuf = strchr(lineBuf, '\n'); *cBuf = '\0'; //
        cBuf = strchr(lineBuf, ' ') + 1;            //
        *Buf[i] = malloc(sizeof(char)*strlen(cBuf)+1);
        strcpy(*Buf[i], cBuf);
    } filterDate(log);

}

void filterDate(sLog *log){
    #define SDATE 12 // "xx/xx/xxxx'\0'"
    char *mes;
    int  dia, ano;

    mes = strrchr(log->date, ','); *mes = '\0';    //Delimitando os dados de interrese
    sscanf(log->date, "%s%d,%d", mes, &dia, &ano); //Parsing

    const char *Buf[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    int i;
    for (i = 1; i <= 12; i++)
        if (!strcmp(mes, Buf[i])) break;

    sprintf(log->date, "%d/%d/%d", dia, i, ano);
    log->date = (char*) realloc(log->date, SDATE);
}

//Registro - Resto do arquivo - VARIAVEL GLOBAL avgBuf
#define SCIR 2 //Size Circular queue
typedef struct {
    int cirHr[SCIR], cirCad[SCIR];
    double cirSpd[SCIR];

    int pSpd, pHr, pCad, // 0 ou 1, posicao no buffer
        timestamp, weight,
        iterSpeed, iterHr, iterCadence;
    double altitude;
} avgBuf;

avgBuf GLOBAL = {
    .cirSpd = {0, 0},
    .cirHr = {0, 0},
    .cirCad = {0, 0},
    
    .pSpd = 0, 0, 0,
    .timestamp = 0, 0, 
    .iterSpeed = 0, 0, 0, 
    .altitude = 0,
};

void initGlobal(avgBuf *GLOBAL){
    int i;

    for (i = 0; i < 2; i++)
        GLOBAL->cirSpd[i] = GLOBAL->cirHr[i] = GLOBAL->cirCad[i] = 0;

    GLOBAL->pSpd = GLOBAL->pHr = GLOBAL->pCad =  0;
    GLOBAL->timestamp = GLOBAL->weight = 0;
    GLOBAL->iterSpeed = GLOBAL->iterHr = GLOBAL->iterCadence = 0;
    GLOBAL->altitude = 0;
}

void readRegister(FILE *stream, sLog *log){
    char *inf, *data, *unit;
    char lineBuf[LINESIZE];

    initGlobal(&GLOBAL);

    do {
        fgets(lineBuf, LINESIZE, stream);
        inf = strtok(lineBuf, ":\n"); data = strtok(NULL, " \n"); unit = strtok(NULL, " \n");
        switchLog(inf, data, unit, log);
    } while (!feof(stream)); //Fim da stream
    filterRegister(log);
}

//Switch Log

//Switch Case ou Function pointer? - Union para prototipos diferentes ou Wrapper?
//Modularizao mesmo com o overhead de memoria?...
//Faz Sentido mandar buffer e estrutura? Pq nao criar a estrutura no final
//void (*fp[])(sLog *log, double data, char *unit) = {switchAlt, switchCad, switchDist,
//                                                    switchHr, switchSpd, switchTmp};
//typedef union {
//    void (*fData)(sLog *log, double data); 
//    void (*fUnit)(sLog *log, double data, char *unit);
//} fPointer;
//fPointer fp;

void (*fp[])(sLog *log, double data) = {switchAlt, switchCad, switchDist, switchHr, switchSpd};
void (*fp2)(sLog *log, double data, char *unit) = switchTmp;

void switchLog(char *inf, char *data, char *unit, sLog *log){
    #define SBUF 6
    const char *Buf[] = {"altitude", "cadence", "distance", "heart_rate", "speed", "timestamp"};
    int i;

    if (inf == NULL) return;

    for (i = 0; i < SBUF; i++)
        if (!strcmp(inf, Buf[i])) break;

    if (i == SBUF) return; //Nao eh um dado de interesse

    if (i != 5) (*fp[i])(log, atof(data));
    else (*fp2)(log, atof(data), unit);
}

void switchAlt(sLog *log, double data){
    if (data > GLOBAL.altitude && GLOBAL.altitude != 0) log->ascent += data - GLOBAL.altitude;
    GLOBAL.altitude = data;
}

void switchCad(sLog *log, double data){
    GLOBAL.cirCad[GLOBAL.pCad] = data; GLOBAL.pCad ^= 1;
}

void switchDist(sLog *log, double data){
    log->distance = data;
}

void switchHr(sLog *log, double data){
    if (data > log->maxHr) log->maxHr = data; 
    GLOBAL.cirHr[GLOBAL.pHr] = data; GLOBAL.pHr ^= 1;
}

void switchSpd(sLog *log, double data){
    if (data > log->maxSpeed) log->maxSpeed = data;
    GLOBAL.cirSpd[GLOBAL.pSpd] = data; GLOBAL.pSpd ^= 1;
}

void switchTmp(sLog *log, double data, char *unit){
    int h, m, s, time, i;
    double auxSpe;

    sscanf(unit, "%d:%d:%d", &h, &m, &s);
    time = 3600*h + 60*m + s;

    GLOBAL.weight = time - GLOBAL.timestamp;
    
    int *logData[] = {&log->avgHr, &log->avgCad};
    int globalData[] = {GLOBAL.cirHr[GLOBAL.pHr], GLOBAL.cirCad[GLOBAL.pCad]};
    int *globalIter[] = {&GLOBAL.iterHr, &GLOBAL.iterCadence};

    auxSpe = GLOBAL.cirSpd[GLOBAL.pSpd];
    log->avgSpeed += auxSpe * GLOBAL.weight;
    if (auxSpe != 0) GLOBAL.iterSpeed += GLOBAL.weight; //Comparacao de double com 0, cuidado... (int)auxSpe

    for (i = 0; i < 2; i++){
       *logData[i] += globalData[i] * GLOBAL.weight; 
       if (globalData[i]) *globalIter[i] += GLOBAL.weight;
    }

    GLOBAL.timestamp = time;
}

/* Fim das funcoes com variavel global */
void filterRegister(sLog *log){
    log->distance /= 1000; //m para Km
    log->maxSpeed *= 3.6;  //ms/s para km/h

    log->avgSpeed = (log->avgSpeed ? (log->avgSpeed/GLOBAL.iterSpeed) * 3.6 : 0);
    log->avgHr = (log->avgHr ? log->avgHr/GLOBAL.iterHr : 0);
    log->avgCad = (log->avgCad ? log->avgCad/GLOBAL.iterCadence : 0);
}

