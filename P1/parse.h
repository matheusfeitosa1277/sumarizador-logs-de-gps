#ifndef PARSE_H
#define PARSE_H

//Implementacao de um parser usando a funcao
//arg_parse da biblioteca <argp.h>
//Retorna o nome do diretorio inserido em "-d ARG1"
char* parsingArgument(int argc, char **argv);

#endif

