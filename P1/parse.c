#include <stdlib.h>
#include <argp.h>
#include <string.h>
#include "parse.h"
/*Exemplo da documentacao oficial*/

/*Program documentattion. */
static char doc[] =
    "Prog2, T1 - Sumarizacao de Arquivos de Logs de GPS";

/* A description of the arguments we accept. */
static char args_doc[] = "ARG1";

/* The options we understand. */
static struct argp_option options[] = {
  {"diretorio",  'd', 0, 0, 0},
};

/* Used by main to communicate with parse_opt. */
struct arguments
{
  char *args[1];                /* arg1 */
  char *input_dir;
};

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;

  switch (key)
    {
    case 'd':
      arguments->input_dir = arg;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);

      arguments->args[state->arg_num] = arg;

      break;

    case ARGP_KEY_END:
      if (state->arg_num < 1)
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

//Codigo convencional
char* parsingArgument(int argc, char **argv) {
  struct arguments arguments;

  /* Parse our arguments; every option seen by parse_opt will
     be reflected in arguments. */
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  return arguments.args[0]; 
}

