#ifndef DATASCRAPING_H
#define DATASCRAPING_H

#include <dirent.h>

/* Funcoes Auxiliares */
int logExtension(const struct dirent *entry); //Scandir - Filter Function

/* Structs */
//sLog
typedef struct {
    //Header
    char *gear;
    char *date;
    
    //Registers
    double distance, ascent, avgSpeed, maxSpeed;
    int avgHr, maxHr, avgCad;

} sLog; //Summarized Log

sLog *initLog();

void freeLog();

void printLog(sLog *log);

/* ------------- */
/* Data Scraping */
/* ------------- */

sLog *readLog(FILE *stream);

void readHeader(FILE *stream, sLog *log);
void filterDate(sLog *log);

void readRegister(FILE *stream, sLog *log);

void switchLog();
void switchAlt(sLog *log, double data);
void switchCad(sLog *log, double data);
void switchDist(sLog *log, double data);
void switchHr(sLog *log, double data);
void switchSpd(sLog *log, double data);
void switchTmp(sLog *log, double data, char *unit);


void filterRegister(sLog *log);

#endif

