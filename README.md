# Sumarização de Arquivos de Log de GPS

**Enunciado adaptado da Wiki Maziero - http://wiki.inf.ufpr.br/maziero/doku.php?id=start**

Atualmente, a maioria dos ciclocomputadores utilizados pelos ciclistas (amadores e profissionais) possuem um GPS. Esses dispositivos também conversam com diferentes sensores, como por exemplo, sensor de frequência cardíaca, velocidade, cadência, potência, etc. Isso permite que o ciclista/treinador possa analisar os dados do pedal e verificar se o treino foi realizado conforme prescrito.

Todas essas informações (GPS e sensores) são armazenadas a cada segundo. No exemplo abaixo, temos o log de dois segundos. As duas primeiras linhas contém a informação da bicicleta utilizada e a data da atividade. Na sequência, temos diversos campos com os respectivos valores e unidades registrados a cada segundo. Note que nem todos os campos estarão disponíveis. Por exemplo, se o ciclista não usar um sensor de frequência cardíaca, o campo heart_rate não existirá.

![gpsLog](https://www.inf.ufpr.br/msf21/resources/gpsLog.png)

## Atividade

Para esse trabalho, você terá acesso a diversos arquivos de log de diferentes bicicletas. Você deve escrever um programa que leia todos os logs de um dado diretório e apresente um resumo para cada bicicleta da seguinte forma:

![sumarized](https://www.inf.ufpr.br/msf21/resources/sumarized.png)

Ao fim você deve apresentar um sumário contendo as seguintes informações: Quantidade de Atividades, Total Percorrido em km, Pedal mais longo em km, Pedal mais curto em km e Distância Média em km.


## Forma de chamada:

> ./gps -d <diretório de arquivos log>

Com essa chamada, seu programa deve ler todos os arquivos de log encontrados no diretório passado como parâmetro e armazenar as informações relevantes em memória.

Enquanto o programa estiver lendo os arquivos de log, o usuário deve ser informado que o programa está em execução.

Uma vez feito isso, o programa deverá apresentar as seguintes opções ao usuário:

  

-   Bicicletas Encontradas: Mostra todas as bicicletas encontradas durante o processamento dos arquivos de log.
    
-   Pede para o usuário informar uma das bicicletas encontradas e apresenta a lista de atividades, resumo conforme descrito acima.
    
-   Lista todas atividades agrupadas por bicicleta e ordenadas pela data
    
-   Lista todas atividades agrupadas por bicicleta e ordenadas pela distância
    
-   Lista todas atividades ordenadas pela subida acumulada
    
-   Histograma: O usuário deve escolher uma bicicleta e apresentar um histograma representando a distribuição da distância das atividades da bicicleta
